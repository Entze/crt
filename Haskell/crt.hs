{-
MIT License

Copyright (c) 2019 Lukas Grassauer

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
-}

import Data.Ratio
import Data.Numbers.Primes


crtPrecondition :: Integral int => [int] -> [int] -> Bool
crtPrecondition rs ps = pairwiseCoprime ps && crtPrecondition' rs ps
    where
      crtPrecondition' [] [] = True
      crtPrecondition' [] _ = False
      crtPrecondition' _ [] = False
      crtPrecondition' (r:rs) (p:ps) = r < p && crtPrecondition' rs ps

crtPostcondition :: Integral int => [int] -> [int] -> int -> Bool
crtPostcondition rs ps n = n < product ps && and (zipWith (==) rs (zipWith rem (repeat n) ps))

crtNaive :: Integral int => [int] -> [int] -> int
crtNaive [] _ = -1
crtNaive _ [] = -1
crtNaive rs ps
    | crtPrecondition rs ps = headOrDefault (dropWhile (not . crtPostcondition rs ps) candidates) (-1)
    | otherwise = -1
    where
      candidates = [0..(m-1)]
      m = product ps

crtNaiveMin :: Integral int => [int] -> int
crtNaiveMin rs = crtNaive rs (getCrtCoprimes rs)

crt :: Integral int => [int] -> [int] -> int
crt [] _ = -1
crt _ [] = -1
crt rs ps
    | crtPrecondition rs ps = crt' rs ps
    | otherwise = -1
    where
      crt' [r1] [_] = r1
      crt' [r1, r2] [p1, p2] = r'
          where
            r' = if r < 0 then p1 * p2 + r else r
            r = (r2 * s * p1 + r1 * t * p2) `rem` (p1 * p2)
            (s,t) = bezoutCoefficients p1 p2
      crt' (r1:r2:rs) (p1:p2:ps) = crt' (r:rs) ((p1 * p2):ps)
          where
            r' = if r < 0 then p1 * p2 + r else r
            r = crt' (r1:[r2]) (p1:[p2])

crtMin :: Integral int => [int] -> int
crtMin rs = crt rs (getCrtCoprimes rs)

extendedEuclidian :: Integral int => int -> int -> (int, int, int)
extendedEuclidian 0 b = (b, 0, 1)
extendedEuclidian a b = eea 0 1 b 1 0 a
    where
      eea _ _ 0 s' t' r' = (r', s', t')
      eea s t r s' t' r' = eea (s' - q * s) (t' - q * t) (r' - q * r) s t r
          where
            q = r' `quot` r

bezoutCoefficients p1 p2 = (s, t)
    where
      (_, s, t) = extendedEuclidian p1 p2

getCrtCoprimes :: Integral int => [int] -> [int]
getCrtCoprimes [] = []
getCrtCoprimes [r1] = [r1+1]
getCrtCoprimes [r1, r2] = head [[p1, p2] | p1 <- [(r1+1)..], p2 <- [(r2+1)..], relativePrimes p1 p2]
getCrtCoprimes [r1, r2, r3] = head [[p1, p2, p3] | p1 <- [(r1+1)..], p2 <- [(r2+1)..], relativePrimes p1 p2, p3 <- [(r3+1)..], relativePrimes p2 p3]
getCrtCoprimes [r1, r2, r3, r4] = head [[p1, p2, p3, p4] | p1 <- [(r1+1)..], p2 <- [(r2+1)..], relativePrimes p1 p2, p3 <- [(r3+1)..], relativePrimes p2 p3, p4 <- [(r4+1)..], relativePrimes p3 p4]
getCrtCoprimes [r1, r2, r3, r4, r5] = head [[p1, p2, p3, p4, p5] | p1 <- [(r1+1)..], p2 <- [(r2+1)..], relativePrimes p1 p2, p3 <- [(r3+1)..], relativePrimes p2 p3, p4 <- [(r4+1)..], relativePrimes p3 p4, p5 <- [(r5+1)..], relativePrimes p4 p5]
getCrtCoprimes (r:rs) = (r+1):getCrtCoprimes' rs [r+1]
    where
      getCrtCoprimes' [] ps = ps
      getCrtCoprimes' (r:rs) ps@(p:_) = c:(getCrtCoprimes' rs (c:ps))
          where
            (c:_) = [ n | n <- [(r+1)..], relativePrimes p n]


headOrDefault :: [a] -> a -> a
headOrDefault [] d = d
headOrDefault l _ = head l

relativePrimes :: Integral int => int -> int -> Bool
relativePrimes x y = gcd x y == 1

pairwiseCoprime :: Integral int => [int] -> Bool
pairwiseCoprime [] = True
pairwiseCoprime [_] = True
pairwiseCoprime (l1:l2:ls) = relativePrimes l1 l2 && pairwiseCoprime (l2:ls)

----------------------------------------------------------------------------

data Qx = Polynomial [Rational]


instance Num Qx where
    (+) a b = canonical (add a b)
    (*) a b = canonical (mult a b)
    negate a = Polynomial (map negate a')
        where
          (Polynomial a') = canonical a
    fromInteger i = Polynomial [fromInteger i]
    abs = canonical
    signum a = fromInteger 1

instance Show Qx where
    show (Polynomial a) = show'' (reverse a)
        where
          show'' [] = []
          show'' [x]
              | x == fromIntegral 0 = []
              | x > fromIntegral 0 = string
              | otherwise = "-" ++ string
              where
                string
                    | n == 1 && d == 1 = "1"
                    | d == 1 = n'
                    | otherwise = n' ++ '/':d'
                (n, d) = (abs (numerator x), abs (denominator x))
                (n',d') = (show n, show d)
          show'' (x:xs)
              | x == fromIntegral 0 = show'' xs
              | x > fromIntegral 0 = string ++ show' xs
              | x < fromIntegral 0 = "-" ++ string ++ show' xs
              where
                string
                    | n == 1 && d == 1 = "x^" ++ show (length xs)
                    | d == 1 = n' ++ " x^" ++ show (length xs)
                    | otherwise = n' ++ '/':d' ++ " x^" ++ show (length xs)
                (n, d) = (abs (numerator x), abs (denominator x))
                (n',d') = (show n, show d)

          show' [] = []
          show' [x]
              | x == fromIntegral 0 = []
              | x > fromIntegral 0 = " + " ++ string
              | otherwise = " - " ++ string
              where
                string
                    | n == 1 && d == 1 = "1"
                    | d == 1 = n'
                    | otherwise = n' ++ '/':d'
                (n, d) = (abs (numerator x), abs (denominator x))
                (n',d') = (show n, show d)
          show' (x:xs)
              | x == fromIntegral 0 = show' xs
              | x > fromIntegral 0 = " + " ++ string ++ show' xs
              | x < fromIntegral 0 = " - " ++ string ++ show' xs
              where
                string
                    | n == 1 && d == 1 = "x^" ++ show (length xs)
                    | d == 1 = n' ++ " x^" ++ show (length xs)
                    | otherwise = n' ++ '/':d' ++ " x^" ++ show (length xs)
                (n, d) = (abs (numerator x), abs (denominator x))
                (n',d') = (show n, show d)

add :: Qx -> Qx -> Qx
add (Polynomial a) (Polynomial b)
    | la > lb = Polynomial (zipWith (+) a (b ++ extend))
    | otherwise = Polynomial (zipWith (+) (a ++ extend) b)
      where
        la = length a
        lb = length b
        extend = repeat (fromIntegral 0)

multConst :: Qx -> Rational -> Qx
multConst as 0 = fromInteger 0
multConst as 1 = as
multConst (Polynomial as) c = Polynomial (map (* c) as)


mult :: Qx -> Qx -> Qx
mult a (Polynomial [b]) = multConst a b
mult (Polynomial [a]) b = multConst b a
mult (Polynomial a) (Polynomial (0:b)) = mult (Polynomial (0:a)) (Polynomial b)
mult a b = karatsuba a b


canonical :: Qx -> Qx
canonical = removeLeadingZeros

removeLeadingZeros (Polynomial a) = Polynomial ((reverse . (dropWhile (== (fromIntegral 0))) . reverse) a)

setLeadingCoefficentOne a = multConst a i
    where
      i = (d%n)
      (n, d) = (numerator l, denominator l)
      l = lcConst a


karatsuba :: Qx -> Qx -> Qx
karatsuba a b = karatsuba' a b (max (deg a) (deg b))
    where
      karatsuba' (Polynomial a) (Polynomial b) d
          | d <= 0 = (Polynomial a) `mult` (Polynomial b)
          | otherwise = r
          where
            r = m1 + ((m2 - m1 - m3) `mult` (xPow (d2 + 1))) + (m3 `mult` (xPow (2 * d2 + 2)))
            m3 = karatsuba a2 b2
            m2 = karatsuba (a1 + a2) (b1 + b2)
            m1 = karatsuba a1 b1
            a1 = Polynomial (take d2 a)
            a2 = Polynomial (drop d2 a)
            b1 = Polynomial (take d2 b)
            b2 = Polynomial (drop d2 b)
            d2 = d `quot` 2


deg :: Qx -> Int
deg (Polynomial a) = (length a) - 1

xPow :: Int -> Qx
xPow i = Polynomial ((replicate i 0) ++ [1])

denConst :: Qx -> Rational
denConst (Polynomial []) = 1
denConst (Polynomial (a:as)) = denConst' as (denominator a)
    where
      denConst' [] d = fromIntegral d
      denConst' (a:as) d = denConst' as (lcm (denominator a) d)

den :: Qx -> Qx
den a = Polynomial [denConst a]

lc :: Qx -> Qx
lc a = Polynomial [lcConst a]

lcConst :: Qx -> Rational
lcConst (Polynomial a) = last a



modEuclid :: Qx -> Qx -> Qx
modEuclid a b = fromIntegral 0
    where
      a' = a * (den a)
      b' = b * (den b)
      l = gcd (numerator (lcConst a)) (numerator (lcConst b))
